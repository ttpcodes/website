---
title: vgabios dumping on linux
date: 2021-05-03T09:09:06.225Z
summary: "vgabios dump failed due to driver binding: used `/sys` virtual fs to unbind driver before dumping."
projects:
- genbu-ravine
tags:
- vgabios
- vfio
- gpus
---
i needed a working vgabios rom compatible with my gpu to test possible fixes to problems i was encountering with my vfio
setup. most guides point to [techpowerup](https://www.techpowerup.com/vgabios/) as a starting point for finding a
compatible vgabios rom, but i wasn't able to find one compatible with my gpu (turns out msi has many variations of the 
rx 580 armor oc, with different amounts of ram, different clock speeds, and even different vram modules on different 
batches of what appears to be the same gpu model).

that left the option of performing the rom dump myself, and most documentation online points to doing the following
(though i opted to do this from a live cd to avoid existing applications from potentially interfering and just 
restarted once this was done):
```bash
# go to gpu directory under /sys virtual fs
~# cd "/sys/bus/pci/devices/0000:0a:00.0/"
# enable rom read
/sys/bus/pci/devices/0000:0a:00.0# echo 1 > rom
# dump the rom contents
/sys/bus/pci/devices/0000:0a:00.0# cat rom > /usr/share/vgabios/rx580-msi-armoroc-8gb.rom
# disable rom read
/sys/bus/pci/devices/0000:0a:00.0# echo 0 > rom
```
however, i soon found myself running into an error looking like this:
```
cat: rom: Input/output error
```
it turns out that many others online have also run into this issue while attempting to do the same thing. it was only
after excessive poking around that i stumbled upon 
[this thread on the vfio-users mailing list on the redhat website](https://listman.redhat.com/archives/vfio-users/2019-March/msg00004.html)
and was able to figure out how to work around this.

it appears the gpu is unwilling to spit out the rom when there's a driver binded to the gpu. in this case, even though
my passthrough gpu was not used as the primary graphics adapter, the `amdgpu` driver was still bound to it according to
`lspci -k`:
```
0a:00.0 VGA compatible controller: Advanced Micro Devices, Inc. [AMD/ATI] Ellesmere [Radeon RX 470/480] (rev e7)
        Subsystem: Micro-Star International Co., Ltd. [MSI] Ellesmere [Radeon RX 470/480/570/570X/580/580X/590] (Radeon RX 580 Armor 4G OC)
        Kernel driver in use: amdgpu
        Kernel modules: amdgpu
```
luckily, the `/sys` virtual fs makes it possible to unbind the gpu from the driver:
```bash
~# echo -n "0000:0a:00.0" > /sys/bus/pci/drivers/amdgpu/unbind
```
when i ran the command, i encountered the following error in `dmesg`:
```
vfio-pci 0000:07:00.0: Invalid PCI ROM header signature: expecting 0xaa55, got 0xffff
```
however, the output of `lspci -k` showed that `amdgpu` was no longer binded to the gpu:
```
0a:00.0 VGA compatible controller: Advanced Micro Devices, Inc. [AMD/ATI] Ellesmere [Radeon RX 470/480] (rev e7)
        Subsystem: Micro-Star International Co., Ltd. [MSI] Ellesmere [Radeon RX 470/480/570/570X/580/580X/590] (Radeon RX 580 Armor 4G OC)
        Kernel modules: amdgpu
```
with that done, i was able to perform the steps above to dump the vbios.
