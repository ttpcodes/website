---
title: lyra

links:
  github: https://github.com/ttpcodes/lyra
status: inactive
type: personal
update: no plans to polish for proper release
---
web-based 2d music exploration platform
