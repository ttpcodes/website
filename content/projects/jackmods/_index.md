---
title: jackmods

links:
  github: https://github.com/ttpcodes/jackmods
status: hiatus
type: personal
update: currently on hiatus to prioritize other projects
---
web-based jackbox customization generator
