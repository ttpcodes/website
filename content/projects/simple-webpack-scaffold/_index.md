---
title: simple-webpack-scaffold

links:
  gitlab: https://gitlab.com/ttpcodes/simple-webpack-scaffold
status: active
type: personal
update: in need of a rewrite due to webpack scaffolding features being removed
---
simple webpack frontend scaffold
