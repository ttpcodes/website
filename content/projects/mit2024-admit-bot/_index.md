---
title: mit2024-admit-bot

links:
  github: https://github.com/ttpcodes/mit2024-admit-bot
status: maintenance
type: professional
update: currently not actively involved in development but maintaining existing deployments
---
discord bot for authentication of new mit admits to discord servers
