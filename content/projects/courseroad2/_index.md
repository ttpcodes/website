---
title: courseroad2

links:
  github: https://github.com/sipb/courseroad2
  website: https://courseroad.mit.edu
status: hiatus
type: professional
update: currently taking a break from project involvement for personal reasons
---
4-year academic planner for the mit community
