---
title: dotfiles

links:
  gitlab: https://gitlab.com/ttpcodes/dotfiles
status: active
type: experimental
update: currently adding scripts for base system components
---
personal dotfiles
