---
title: about
---
my name is alice (also known as `ttpcodes` online).
i’m a 20-year-old undergraduate student at the massachusetts institute of technology,
currently pursuing a major in electrical engineering & computer science and a minor in japanese.

i use she/her pronouns.

## status
- :books: studying remotely in the boston area

## other info
- the art used in my profile picture is by [@lanalopez92](https://twitter.com/lanalopez92).
  the character depicted is [nitori kawashiro](https://en.touhouwiki.net/wiki/Nitori_Kawashiro),
  a kappa and engineer from the [touhou project](https://en.touhouwiki.net/wiki/Touhou_Project) series.
