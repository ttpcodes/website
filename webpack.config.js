const { resolve } = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const SvgStorePlugin = require('external-svg-sprite-loader');
module.exports = {
	entry: {
		appJs: './src/js/app.js'
	},

	module: {
		rules: [
			{
				test: /\.css$/,

				use: [
					{
						loader: MiniCssExtractPlugin.loader
					},
					'css-loader'
				]
			},
			{
				test: /\.(woff(2)?|ttf|eot)(\?v=\d+\.\d+\.\d+)?$/,

				use: [
					{
						loader: 'file-loader',

						options: {
							name: '[name].[ext]',
							outputPath: './fonts',
							publicPath: '../fonts'
						}
					}
				]
			},
			{
				test: /\.svg$/,

				use: [
					{
						loader: SvgStorePlugin.loader,

						options: {
							name: 'svg/sprite.svg',
							publicPath: '../'
						}
					}
				]
			}
		]
	},

	output: {
		filename: 'js/app.js',
		path: resolve(__dirname, 'dist')
	},

	plugins: [
		new MiniCssExtractPlugin({
			filename: 'css/app.css'
		}),
		new SvgStorePlugin()
	]
};
