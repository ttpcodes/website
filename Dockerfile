FROM node:14.16.0-alpine3.13 as bundle

COPY . /srv/
WORKDIR /srv/
RUN yarn install
RUN yarn run production

FROM klakegg/hugo:0.82.0-alpine-ci as build

COPY --from=bundle /srv/ /src/
RUN hugo

FROM nginx:1.19.9-alpine

COPY --from=build /src/public/ /usr/share/nginx/html/
